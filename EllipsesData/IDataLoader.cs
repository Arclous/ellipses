﻿using System.Data;

namespace EllipsesData
{
    public interface IDataLoader
    {
        DataSet LoadData(string fileName, string sheet, string[] columns = null);
        DataSet LoadDataFromTextFile(string fileName, string[] columns);
    }
}