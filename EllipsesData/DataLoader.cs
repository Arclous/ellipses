﻿using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace EllipsesData
{
    public class DataLoader : IDataLoader
    {
        /// <summary>
        ///     Loads data from the file
        /// </summary>
        /// <param name="fileName">Filename</param>
        /// <param name="sheet">Name of the sheet</param>
        /// <param name="columns">Columns which needs to fetch,if it is null all columns will be fetched </param>
        public DataSet LoadData(string fileName, string sheet, string[] columns = null)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException();

            var connStr =
                string.Format(
                    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";",
                    fileName);

            //Create dataset
            var dataSet = new DataSet();
            dataSet.Tables.Add(new DataTable(sheet));
            var table = dataSet.Tables[0];

            if (columns != null)
                for (var i = 0; i <= columns.Length - 1; i++)
                    table.Columns.Add(columns[i]);

            var items = new List<object>();
            using (var conn = new OleDbConnection(connStr))
            {
                conn.Open();
                var sql = string.Format(@"SELECT * FROM [{0}$]", sheet);
                var cmd = new OleDbCommand(sql, conn);
                var reader = cmd.ExecuteReader();

                if (columns == null)
                    for (var i = 0; i <= reader.FieldCount - 1; i++)
                        table.Columns.Add(reader.GetName(i));

                while (reader != null && reader.Read())
                    if (columns != null)
                    {
                        items.Clear();
                        for (var d = 0; d <= table.Columns.Count - 1; d++)
                            items.Add(reader[d]);

                        var dataRow = table.NewRow();
                        dataRow.ItemArray = items.ToArray();
                        table.Rows.Add(dataRow);
                    }
                    else
                    {
                        items.Clear();
                        for (var d = 0; d <= reader.FieldCount - 1; d++)
                            items.Add(reader[d]);

                        var dataRow = table.NewRow();
                        dataRow.ItemArray = items.ToArray();
                        table.Rows.Add(dataRow);
                    }

                return dataSet;
            }
        }

        /// <summary>
        ///     Loads data from the txt file, data items should be seperated with ',' comma.
        /// </summary>
        /// <param name="fileName">Filename</param>
        /// <param name="columns">Columns which needs to fetch,if it is null all columns will be fetched </param>
        public DataSet LoadDataFromTextFile(string fileName, string[] columns)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException();

            var dataSet = new DataSet();
            dataSet.Tables.Add(new DataTable());
            var table = dataSet.Tables[0];

            if (columns != null)
                for (var i = 0; i <= columns.Length - 1; i++)
                    table.Columns.Add(columns[i]);

            var items = new List<object>();

            using (var fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var bs = new BufferedStream(fs))
                    using (var sr = new StreamReader(bs))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            var vals = line.Split(',');
                            items.Clear();
                            for (var i = 0; i <= vals.Length - 1; i++)
                                items.Add(vals[i]);
                            
                            var dataRow = table.NewRow();
                            dataRow.ItemArray = items.ToArray();
                            table.Rows.Add(dataRow);
                        }
                    }

            return dataSet;
        }


        /// <summary>
        ///     Loads data from the txt file, data items should be seperated with ',' comma.
        /// </summary>
        /// <param name="fileName">Filename</param>
        /// <param name="columns">Columns which needs to fetch,if it is null all columns will be fetched </param>
        public double[][] LoadArrayData(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException();

            List<double[]> dataList=new List<double[]>();

            var items = new List<double>();

            using (var fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var bs = new BufferedStream(fs))
            using (var sr = new StreamReader(bs))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var vals = line.Split(',');
                    items.Clear();
                    for (var i = 0; i <= vals.Length - 1; i++)
                        items.Add(double.Parse(vals[i]));


                    dataList.Add(items.ToArray());

                }
            }

            return dataList.ToArray();
        }

    }
}