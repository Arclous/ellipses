﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ellipses.Algorithms;
using Ellipses.Metrics;
using EllipsesExamples.Helpers;
using EllipsesExamples.Models;

namespace EllipsesExamples.Dialogs
{
    public partial class DlgNaiveBayes : Form
    {
        public DlgNaiveBayes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double[][] arrayDoubles = new double[10][];
            for (int i = 0; i < 10; i++)
            {
                arrayDoubles[i] = new double[3];
                for (int j = 0; j < 3; j++)

                    arrayDoubles[i][j] =i*j;

                   
            }
          

               var list = new List<NbModel>();
            var nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Label = DataHelper.GetLabel("go-out") , Test = arrayDoubles};
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("rainy"), Car = DataHelper.GetCar("broken"), Label = DataHelper.GetLabel("go-out"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Label = DataHelper.GetLabel("go-out"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Label = DataHelper.GetLabel("go-out"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Label = DataHelper.GetLabel("go-out"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("rainy"), Car = DataHelper.GetCar("broken"), Label = DataHelper.GetLabel("stay-home"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("rainy"), Car = DataHelper.GetCar("broken"), Label = DataHelper.GetLabel("stay-home"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Label = DataHelper.GetLabel("stay-home"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("broken"), Label = DataHelper.GetLabel("stay-home"), Test = arrayDoubles };
            list.Add(nbModel);
            nbModel = new NbModel() { Weather = DataHelper.GetWeather("rainy"), Car = DataHelper.GetCar("broken"), Label = DataHelper.GetLabel("stay-home"), Test = arrayDoubles };
            list.Add(nbModel);

            var naiveBayes = new NaiveBayes();
            naiveBayes.LoadDataSet(list.ToArray());
            var predicter = naiveBayes.Train();

            nbModel = new NbModel() { Weather = DataHelper.GetWeather("sunny"), Car = DataHelper.GetCar("working"), Test = arrayDoubles };
            var test = predicter.Predict(nbModel);

        }

        private void DlgNaiveBayes_Load(object sender, EventArgs e)
        {

        }
    }
}
