﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ellipses.Algorithms;
using Ellipses.Interfaces;
using EllipsesData;
using EllipsesExamples.Helpers;
using EllipsesExamples.Models;

namespace EllipsesExamples.Dialogs
{
    public partial class DlgApriori : Form
    {
        private IAprioriPredicter predicter;
        public DlgApriori()
        {
            InitializeComponent();
        }

        private void DlgApriori_Load(object sender, EventArgs e)
        {
            var transactions = new List<Transaction>();
            var dataLoader = new DataLoader();
            var data = dataLoader.LoadArrayData(@"C:\Users\Ali\Desktop\data.txt");

            var index = 0;
            foreach (var dtr in data)
            {
                index += 1;
                var inputList = new List<double[]> {dtr};
                var transaction = new Transaction
                {
                    Id = index,
                    Products = inputList.ToArray()
                };
                transactions.Add(transaction);

            }
            
            //var apriori = new Apriori(60);
            //apriori.LoadDataSet<Transaction>(transactions.ToArray());
            //var predicter = apriori.Train();
            //var test = predicter.Predict(new double[] { 11 });

            SetDictionary();
            List<string> Words=new List<string>();
            Words.Add("Ali");
            Words.Add("Gulum");
            Words.Add("Avrupa");
            Words.Add("Ali Gulum");
            Words.Add("Muhammet Ali");
            Words.Add("Albayrak");
            Words.Add("Alacak");
            Words.Add("Basketbol");
            Words.Add("Alice");
            Words.Add("Alim");
            Words.Add("Toprak Gulum");
            Words.Add("Tatis Gulum");
            Words.Add("Ayhan Gulum");
            Words.Add("Nilufer Gulum");
            Words.Add("Ozlem Gulum");
            Words.Add("Bul");
            Words.Add("Barcelona");
            Words.Add("Buyuk");

            transactions.Clear();
            List<double[]> ids=new List<double[]>();

            foreach (var v in Words)
            {
                ids.Clear();
                ids.Add(GetIds(v));
               var transaction = new Transaction
               {
                   Id = index,
                   Products = ids.ToArray()
               };
                transactions.Add(transaction);

            }

            var apriori = new Apriori(30);
            apriori.LoadDataSet<Transaction>(transactions.ToArray());
            predicter = apriori.Train();
           // var test = predicter.Predict(new double[] { 1 });
        }

        private Dictionary<string, double> IdList = new Dictionary<string, double>();
        public void SetDictionary()
        {
            IdList.Add("a",1);
            IdList.Add("b", 2);
            IdList.Add("c", 3);
            IdList.Add("d", 4);
            IdList.Add("e", 5);
            IdList.Add("f", 6);
            IdList.Add("g", 7);
            IdList.Add("h", 8);
            IdList.Add("i", 9);
            IdList.Add("j", 10);
            IdList.Add("k", 11);
            IdList.Add("l", 12);
            IdList.Add("m", 13);
            IdList.Add("n", 14);
            IdList.Add("o", 15);
            IdList.Add("p", 16);
            IdList.Add("q", 17);
            IdList.Add("r", 18);
            IdList.Add("s", 19);
            IdList.Add("t", 20);
            IdList.Add("u", 21);
            IdList.Add("v", 22);
            IdList.Add("w", 23);
            IdList.Add("x", 24);
            IdList.Add("y", 25);
            IdList.Add("z", 26);
            IdList.Add(" ", 27);
        }

        private double[] GetIds(string value)
        {
            var letters = value.ToLower().ToArray();
            return letters.Select(letter => IdList[letter.ToString()]).ToArray();
        }

        private string GetLetter(double id)
        {
            return IdList.FirstOrDefault(x=>x.Value==id).Key;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var ids = GetIds(textBox1.Text);
            var test = predicter.Predict(new double[] { ids[ids.Count()-1] });

            var topList = test.ExactSet.OrderByDescending(x => x.Item3).Where(x=>x.Item1.Length==1).Take(1);

            textBox2.Text = "";
            foreach (var v in topList)
            {
                string suggest = "";
                for (int j = 0; j < v.Item2.Length; j++)
                {
                    suggest = string.Format(@"{0}{1}", textBox1.Text, GetLetter(v.Item2[j]));
                    

                    var test2 = predicter.Predict(new double[] { v.Item2[j] });
                    var topList2 = test2.ExactSet.OrderByDescending(x => x.Item3).Where(x => x.Item1.Length == 1).Take(1);
                    foreach (var v2 in topList2)
                    {

                        var test3 = predicter.Predict(new double[] { v.Item2[j] });
                        var topList3 = test3.ExactSet.OrderByDescending(x => x.Item3).Where(x => x.Item1.Length == 1).Take(1);

                        for (int j2 = 0; j2 < v2.Item2.Length; j2++)
                        {
                            var suggest2 = string.Format(@"{0}{1}", suggest, GetLetter(v2.Item2[j]));
                            textBox2.Text += string.Format(@"{0}{1}", Environment.NewLine, suggest2);
                        }
                    }

         
                }

                }
        }
    }
}
