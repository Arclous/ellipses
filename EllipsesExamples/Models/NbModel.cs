﻿using Ellipses.Helpers.Attributes;

namespace EllipsesExamples.Models
{
    public class NbModel
    {
        [AiField]
        public double Weather { get; set; }

        [AiField]
        public double Car { get; set; }

        [AiLabel]
        public double Label { get; set; }

      
        public double[][] Test { get; set; }
    }
}