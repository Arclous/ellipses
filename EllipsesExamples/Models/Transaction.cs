﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellipses.Helpers.Attributes;

namespace EllipsesExamples.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        [AiDimensionalFieldAttribute]
        public double[][] Products { get; set; }
    }
}
