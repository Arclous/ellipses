﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EllipsesExamples.Helpers
{
    public static class DataHelper
    {
        public static double GetWeather(string val)
        {
            return val == "sunny" ? 1 : 0;
        }
        public static double GetCar(string val)
        {
            return val == "working" ? 1 : 0;
        }

        public static double GetLabel(string val)
        {
            return val == "go-out" ? 1 : 0;
        }

        public static int GetFlowerValue(string val)
        {
            switch (val)
            {
                case "Iris-setosa":
                    return 0;
                case "Iris-versicolor":
                    return 1;
                case "Iris-virginica":
                    return 2;
            }

            return 0;
        }
    }
}
