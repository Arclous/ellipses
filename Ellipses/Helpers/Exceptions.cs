﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellipses.Helpers
{
    [Serializable]
    public class DataException : ArgumentException
    {
        public DataException(string message) :
            base(message) { }

        public DataException(string paramName, string message) :
            base(message, paramName) { }
    }
}
