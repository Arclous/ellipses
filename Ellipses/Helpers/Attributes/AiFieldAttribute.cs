﻿using System;

namespace Ellipses.Helpers.Attributes
{
    /// <summary>
    ///     Defines a property or field as an attribute to use for the k-means clustering
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class AiFieldAttribute : Attribute
    {
    }
    /// <summary>
    ///     Defines a property or field as an attribute to use for the k-means clustering
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class AiDimensionalFieldAttribute : Attribute
    {
    }
    /// <summary>
    ///     Defines a property or field as an attribute to use for the k-means clustering
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class AiLabelAttribute : Attribute
    {
    }
}