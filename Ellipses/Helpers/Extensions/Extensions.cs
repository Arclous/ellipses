﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Ellipses.Helpers.Extensions
{
    public static class Extensions
    {
        public static T Value<T>(this DataRow datarow, string columnName)
        {
            if (datarow[columnName] != DBNull.Value)
                return (T) Convert.ChangeType(datarow[columnName], typeof (T));
            return (T) Convert.ChangeType(0, typeof (T));
        }

        public static T Value<T>(this DataRow datarow, int columnIndex)
        {
            if (datarow[columnIndex] != DBNull.Value)
                return (T) Convert.ChangeType(datarow[columnIndex], typeof (T));
            return (T) Convert.ChangeType(0, typeof (T));
        }

    
    }
}