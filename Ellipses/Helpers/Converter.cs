﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ellipses.Helpers.Attributes;
using Ellipses.Interfaces;
using Ellipses.Metrics;

namespace Ellipses.Helpers
{
    public class Converter : IConverter
    {
        /// <summary>
        ///     Prepares models for processing
        /// </summary>
        /// <param name="items">Model items</param>
        public double[][] ConvertModels<T>(IReadOnlyList<T> items)
        {
            IList list = items.ToList();
            return GetValuesFor<T>(ref list, typeof (AiFieldAttribute)).ToArray();
        }

        /// <summary>
        ///     Prepares models for processing
        /// </summary>
        /// <param name="items">Model items</param>
        public double[][] ConvertDimensionalModels<T>(IReadOnlyList<T> items)
        {
            IList list = items.ToList();
            return GetValuesFor<T>(ref list, typeof (AiDimensionalFieldAttribute), true).ToArray();
        }

        /// <summary>
        ///     Prepares model for processing
        /// </summary>
        /// <param name="model">Model item</param>
        public double[][] ConvertModel<T>(T model)
        {
            var mod = model;
            return GetValuesFromModel(ref mod, typeof (AiFieldAttribute)).ToArray();
        }

        /// <summary>
        ///     Prepares model for processing
        /// </summary>
        /// <param name="model">Model item</param>
        public double[][] ConvertDimensionalModel<T>(T model)
        {
            var mod = model;
            return GetValuesFromModel(ref mod, typeof (AiDimensionalFieldAttribute), true).ToArray();
        }

        /// <summary>
        ///     Prepares models for processing as matrix
        /// </summary>
        /// <param name="items">Model items</param>
        public Matrix ConvertModelsAsMatrix<T>(IReadOnlyList<T> items)
        {
            IList list = items.ToList();
            return new Matrix(GetValuesFor<T>(ref list, typeof (AiFieldAttribute)).ToArray());
        }

        /// <summary>
        ///     Prepares models for processing as matrix
        /// </summary>
        /// <param name="items">Model items</param>
        public Matrix ConvertLabelsAsMatrix<T>(IReadOnlyList<T> items)
        {
            IList list = items.ToList();
            return new Matrix(GetValuesFor<T>(ref list, typeof (AiLabelAttribute)).ToArray());
        }

        /// <summary>
        ///     Checks the model if it has deminasional fields
        /// </summary>
        /// <param name="items">Model items</param>
        public bool IsDimensionalFieldExist<T>(IReadOnlyList<T> items)
        {
            var properties = PropertyInfos<T>(BindingFlags.Instance | BindingFlags.Public);
            return GetMethodInfos(properties, typeof (AiDimensionalFieldAttribute)).Count > 0;
        }

        /// <summary>
        ///     Checks the model if it has deminasional fields
        /// </summary>
        /// <param name="item">Model item</param>
        public bool IsDimensional<T>(T item)
        {
            var properties = PropertyInfos<T>(BindingFlags.Instance | BindingFlags.Public);
            return GetMethodInfos(properties, typeof (AiDimensionalFieldAttribute)).Count > 0;
        }

        #region Helpers     
        /// <summary>
        ///     Get Values from items according to attributeType
        /// </summary>
        /// <param name="items">Model items</param>
        /// <param name="attributeType">Type to check</param>
        /// <param name="dimensional">Is type dimensional</param>
        private static IEnumerable<double[]> GetValuesFor<T>(ref IList items, Type attributeType,
            bool dimensional = false)
        {
            var properties = PropertyInfos<T>(BindingFlags.Instance | BindingFlags.Public);
            var methodInfos = GetMethodInfos(properties, attributeType);
            if (dimensional)
                return (IList<double[]>) GetMethodsValuesDimensional(ref items, methodInfos);

            return (IList<double[]>) GetMethodsValues(ref items, methodInfos);
        }

        /// <summary>
        ///     Get Values from item according to attributeType
        /// </summary>
        /// <param name="model">Model item</param>
        /// <param name="attributeType">Type to check</param>
        /// <param name="dimensional">Is type dimensional</param>
        private static IEnumerable<double[]> GetValuesFromModel<T>(ref T model, Type attributeType,
            bool dimensional = false)
        {
            var properties = PropertyInfos<T>(BindingFlags.Instance | BindingFlags.Public);
            var methodInfos = GetMethodInfos(properties, attributeType);
            if (dimensional)
                return (IList<double[]>) GetMethodsValuesDimensionalFromModel(ref model, methodInfos);

            return (IList<double[]>) GetMethodsValuesFromModel(ref model, methodInfos);
        }

        /// <summary>
        ///     Gets property infos
        /// </summary>
        /// <param name="bindingFlags">Binding flags</param>
        private static PropertyInfo[] PropertyInfos<T>(BindingFlags bindingFlags)
        {
            var type = typeof (T);
            return type.GetProperties(bindingFlags);
        }

        /// <summary>
        ///     Gets method infos
        /// </summary>
        /// <param name="properties">Properties</param>
        /// <param name="attributeType">Attribute Type to get</param>
        private static List<MethodInfo> GetMethodInfos(IReadOnlyList<PropertyInfo> properties, Type attributeType)
        {
            var methodInfos = new List<MethodInfo>();
            for (var index = 0; index < properties.Count; index++)
            {
                var feature = properties[index];
                var attribs =
                    feature.GetCustomAttributes(attributeType, false).Where(x => x.GetType() == attributeType).ToArray();
                if (attribs.Length <= 0) continue;
                var getter = feature.GetGetMethod();
                if (getter == null)
                    throw new InvalidOperationException("No public getter for property '" + feature.Name +
                                                        "'. All properties marked with the Custom Attiribute must have a public getter");
                methodInfos.Add(getter);
            }
            return methodInfos;
        }

        /// <summary>
        ///     Gets method values
        /// </summary>
        /// <param name="items">Items for geting values</param>
        /// <param name="methodInfos">Method infos</param>
        private static IEnumerable GetMethodsValues(ref IList items, IReadOnlyCollection<MethodInfo> methodInfos)
        {
            var data = new List<double[]>();
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var values = new List<double>(methodInfos.Count);
                values.AddRange(methodInfos.Select(getter => Convert.ToDouble(getter.Invoke(item, null))));
                data.Add(values.ToArray());
            }
            return data.ToArray();
        }

        /// <summary>
        ///     Gets method values
        /// </summary>
        /// <param name="model">Item for geting values</param>
        /// <param name="methodInfos">Method infos</param>
        private static IEnumerable GetMethodsValuesFromModel<T>(ref T model, IReadOnlyCollection<MethodInfo> methodInfos)
        {
            var data = new List<double[]>();
            var item = model;
            var values = new List<double>(methodInfos.Count);
            values.AddRange(methodInfos.Select(getter => Convert.ToDouble(getter.Invoke(item, null))));
            data.Add(values.ToArray());
            return data.ToArray();
        }

        /// <summary>
        ///     Gets method values
        /// </summary>
        /// <param name="items">Dimensional Items for geting values</param>
        /// <param name="methodInfos">Method infos</param>
        private static IEnumerable GetMethodsValuesDimensional(ref IList items, IReadOnlyList<MethodInfo> methodInfos)
        {
            var data = new List<double[]>();
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var value = methodInfos.Select(getter => (double[][]) (getter.Invoke(item, null))).ToList();
                data.AddRange(value[0]);
               // data.Add(value[0][i]);
            }
            return data.ToArray();
        }

        /// <summary>
        ///     Gets method values
        /// </summary>
        /// <param name="model">Dimensional  for geting values</param>
        /// <param name="methodInfos">Method infos</param>
        private static IEnumerable GetMethodsValuesDimensionalFromModel<T>(ref T model,
            IEnumerable<MethodInfo> methodInfos)
        {
            var data = new List<double[]>();
            var item = model;
            var value = methodInfos.Select(getter => (double[][]) (getter.Invoke(item, null))).ToList();
            for (var i = 0; i < value[0].Length; i++)
            {
                data.Add(value[0][i]);
            }

            return data.ToArray();
        }
        #endregion
    }
}