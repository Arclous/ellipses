﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System.Collections.Generic;
using System.Linq;
using Ellipses.Interfaces;
using Ellipses.Metrics;

namespace Ellipses.Helpers
{
    public class Normalizer : INormalizer
    {
        private double[] _max;
        private double[] _min;

        /// <summary>
        ///     Normalize data set
        /// </summary>
        /// <param name="dataSet">Data set</param>
        public double[][] Normalize(double[][] dataSet)
        {
            var listMin = new List<double>();
            var listMax = new List<double>();

            var dataSetN = dataSet;

            var firstOrDefault = dataSetN.FirstOrDefault();
            if (firstOrDefault == null) return null;
            var featuresCount = firstOrDefault.Length;

            for (var di = 0; di <= featuresCount - 1; di++)
            {
                var min = dataSetN.Select(x => x[di]).Min();
                var max = dataSetN.Select(x => x[di]).Max();

                listMin.Add(min);
                listMax.Add(max);
                var difference = max - min;
                for (var i = 0; i <= dataSet.Length - 1; i++)
                {
                    var x = dataSetN[i];
                    var val = x[di];
                    x[di] = (val - min) / difference;
                }
            }
            _min = listMin.ToArray();
            _max = listMax.ToArray();

            return dataSetN;
        }

        public Matrix NormalizeAsMatrix(double[][] dataSet)
        {
            var listMin = new List<double>();
            var listMax = new List<double>();

            var dataSetN = dataSet;

            var firstOrDefault = dataSetN.FirstOrDefault();
            if (firstOrDefault == null) return null;
            var featuresCount = firstOrDefault.Length;

            for (var di = 0; di <= featuresCount - 1; di++)
            {
                var min = dataSetN.Select(x => x[di]).Min();
                var max = dataSetN.Select(x => x[di]).Max();

                listMin.Add(min);
                listMax.Add(max);
                var difference = max - min;
                for (var i = 0; i <= dataSet.Length - 1; i++)
                {
                    var x = dataSetN[i];
                    var val = x[di];
                    x[di] = (val - min) / difference;
                }
            }
            _min = listMin.ToArray();
            _max = listMax.ToArray();

            return new Matrix(dataSetN);
        }

        public Matrix MatrixNormalization(Matrix dataSet)
        {
            var listMin = new List<double>();
            var listMax = new List<double>();

            var dataSetN = dataSet;


            var firstOrDefault = dataSetN.GetFirst();
            if (firstOrDefault == null) return null;
            var featuresCount = firstOrDefault.Length;

            for (var di = 0; di <= featuresCount - 1; di++)
            {
                var di1 = di;
                var min = dataSetN.Select(x => x[di1]).Min();
                var di2 = di;
                var max = dataSetN.Select(x => x[di2]).Max();

                listMin.Add(min);
                listMax.Add(max);
                var difference = max - min;
                for (var i = 0; i <= dataSet.Rows - 1; i++)
                {
                    var x = dataSetN[i];
                    var val = x[di];
                    x[di] = (val - min) / difference;
                }
            }
            _min = listMin.ToArray();
            _max = listMax.ToArray();

            return dataSetN;
        }

        /// <summary>
        ///     Normalize data
        /// </summary>
        /// <param name="y">Data set</param>
        public double[] NormalizeInput(IReadOnlyList<double> y)
        {
            var normalizedData = new List<double>();
            for (var di = 0; di <= y.Count - 1; di++)
            {
                var min = _min[di];
                var max = _max[di];
                var difference = max - min;
                var nVal = (y[di] - min) / difference;
                normalizedData.Add(nVal);
            }
            return normalizedData.ToArray();
        }
    }
}