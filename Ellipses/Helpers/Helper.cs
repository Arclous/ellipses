﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using Ellipses.Definations;
using Ellipses.Helpers.Extensions;
using Ellipses.Interfaces;
using Ellipses.Metrics;
using Ellipses.Models;

namespace Ellipses.Helpers
{
    public class Helper : IHelper
    {
        /// <summary>
        ///     Initializes Clusters
        /// </summary>
        /// <param name="numData">Number of the data</param>
        /// <param name="clusterCount">Number of the cluster</param>
        /// <param name="seed">Seed</param>
        public int[] InitializeClustering(int numData, int clusterCount, int seed)
        {
            var rnd = new Random(seed);
            var clustering = new int[numData];
            for (var i = 0; i < numData; ++i)
                clustering[i] = rnd.Next(0, clusterCount);
            return clustering;
        }

        /// <summary>
        ///     Create Matrix
        /// </summary>
        /// <param name="rows">Rows</param>
        /// <param name="columns">Columns</param>
        public double[][] BuildMatrixAsArray(int rows, int columns)
        {
            var matrix = new double[rows][];
            for (var i = 0; i < matrix.Length; i++)
                matrix[i] = new double[columns];
            return matrix;
        }

        /// <summary>
        ///     Create Matrix
        /// </summary>
        /// <param name="rows">Rows</param>
        /// <param name="columns">Columns</param>
        public Matrix BuildMatrix(int rows, int columns)
        {
            return new Matrix(rows, columns);
        }

        /// <summary>
        ///     Organize clusters with labels
        /// </summary>
        /// <param name="result">Cluster result referance</param>
        /// <param name="labels">Labels</param>
        /// <param name="order">Order, as default x=>x</param>
        public Dictionary<string, List<T>> OrganizeClustersWithLabel<T>(ref KMeansResults<T> result, string[] labels,
            Func<int, int> order = null)
        {
            var dataResult = new Dictionary<string, List<T>>();
            if (order == null)
                order = x => x;
            if (labels.Length != result.Centroids.Length)
                throw new InvalidDataException();
            var i = 0;
            foreach (var v in result.Centroids.OrderBy(order))
            {
                var title = labels[i];
                var index = GetIndex(result.Centroids, v);
                dataResult.Add(title, result.Clusters[index].ToList());
                i += 1;
            }
            return dataResult;
        }

        /// <summary>
        ///     Variance
        /// </summary>
        /// <param name="data">Data array</param>
        /// <param name="mean">Mean</param>
        public double Variance(double[] data, double mean)
        {
            var deviation = new double[data.Count()];
            for (var i = 0; i < data.Count(); i++)
                deviation[i] = Math.Pow(data[i] - mean, 2);
            var variance = deviation.Average();
            return variance;
        }

        /// <summary>
        ///     Variance
        /// </summary>
        /// <param name="x">Data</param>
        /// <param name="mean">Mean</param>
        /// <param name="std">Std</param>
        public double Gauss(double x, double mean, double std)
        {
            var tmp = 1 / (Math.Sqrt(2 * Math.PI) * std);
            //#TODO Delete if doesnt need
            var val1 = tmp * Math.Exp(-.5 * Math.Pow((x - mean) / std, 2));
            var value = 1 / Math.Sqrt(2 * Math.PI * std);
            var value3 = -Math.Pow(x - mean, 2) / (2 * std);
            var val = value * Math.Exp(value3);
            if (val > 1 || val < 0)
                throw new Exception("Probability has to stay between 0 and 1");
            return val;
        }

        #region Helpers

        private static int GetIndex(IReadOnlyList<int> centroids, int value)
        {
            for (var i = 0; i <= centroids.Count - 1; i++)
                if (centroids[i] == value) return i;
            return -1;
        }

        /// <summary>
        ///     Saves model
        /// </summary>
        /// <typeparam name="T">Type of the model</typeparam>
        /// <param name="fileName">Filename for saving trained model</param>
        /// <param name="model">Model tos save</param>
        public void SaveModel<T>(string fileName, T model)
        {
            if (File.Exists(fileName)) File.Delete(fileName);

            using (Stream stream = File.Open(fileName, FileMode.Create))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, model);
            }
        }
        /// <summary>
        ///     Loads trained models
        /// </summary>
        /// <typeparam name="T">Type of the model</typeparam>
        /// <param name="fileName">Filename of the trained model</param>
        /// <returns></returns>
        public T LoadModel<T>(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException();

            using (Stream stream = File.Open(fileName, FileMode.Open))
            {
                var binaryFormatter = new BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        #endregion

        #region StringHelpers

        private static IEnumerable<string> GetChars(string s, IReadOnlyCollection<string> exclusions = null)
        {
            s = s.Trim().ToUpperInvariant();
            foreach (var a in s)
            {
                var key = a.ToString();

                // ignore whitespace (should maybe set as option? I think it's noise)
                if (string.IsNullOrWhiteSpace(key)) continue;

                // ignore excluded items
                if (exclusions != null && exclusions.Count > 0 && exclusions.Contains(key))
                    continue;

                // make numbers and symbols a single feature
                // I think it is noise....
                key = char.IsSymbol(a) || char.IsPunctuation(a) || char.IsSeparator(a) ? "#SYM#" : key;
                key = char.IsNumber(a) ? "#NUM#" : key;
                yield return key;
            }
        }

        private static IEnumerable<string> GetWords(string s, string separator,
            IReadOnlyCollection<string> exclusions = null)
        {
            s = s.Trim().ToUpperInvariant();
            foreach (var w in s.Split(separator.ToCharArray()))
            {
                var key = w.Trim();

                // kill inlined stuff that creates noise
                // (like punctuation etc.)
                key = key.Aggregate("", (x, a) =>
                {
                    if (char.IsSymbol(a) || char.IsPunctuation(a) || char.IsSeparator(a))
                        return x;
                    return x + a.ToString();
                });

                // null or whitespace
                if (string.IsNullOrWhiteSpace(key)) continue;

                // if stemming or anything of that nature is going to
                // happen, it should happen here. The exclusion dictionary
                // should also be modified to take into account the 
                // excluded terms

                // in excluded list
                if (exclusions != null && exclusions.Count > 0 && exclusions.Contains(key))
                    continue;

                // found a number! decimal pointed numbers should work since we
                // killed all of the punctuation!
                key = key.Where(c => char.IsNumber(c)).Count() == key.Length ? "#NUM#" : key;
                yield return key;
            }
        }

        #endregion
    }
}