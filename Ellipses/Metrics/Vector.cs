﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ellipses.Definations;

namespace Ellipses.Metrics
{
    public class Vector : IEnumerable<double>
    {
        private readonly double[] _vectorValues;
        private double[][] _matrix;
        private int _staticIdx = -1;

        /// <summary>
        ///     this is when the values are actually referencing
        ///     a vector in an existing matrix
        /// </summary>
        /// <param name="m">private matrix vals</param>
        /// <param name="idx">static col reference</param>
        internal Vector(double[][] m, int idx)
        {
            _matrix = m;
            _staticIdx = idx;
        }

        /// <summary>
        ///     Creates vector with number if items, items will be 0 as default
        /// </summary>
        /// <param name="count">Total number</param>
        private Vector(int count)
        {
            _vectorValues = new double[count];
            for (var i = 0; i < count; i++)
                _vectorValues[i] = 0;
        }

        /// <summary>
        ///     Creates vector with values
        /// </summary>
        /// <param name="valueses">Values</param>
        public Vector(double[] valueses)
        {
            _vectorValues = valueses;
        }

        /// <summary>
        ///     Gets the length of the vector
        /// </summary>
        public int Length
        {
            get { return _vectorValues.Length; }
        }

        /// <summary>
        ///     Indexer
        /// </summary>
        public double this[int i]
        {
            get { return _vectorValues[i]; }
            set { _vectorValues[i] = value; }
        }

        public double this[Predicate<double> f]
        {
            set
            {
                for (var i = 0; i < Length; i++)
                    if (f(this[i]))
                        this[i] = value;
            }
        }

        public IEnumerator<double> GetEnumerator()
        {
            for (var i = 0; i < Length; i++)
                yield return this[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (var i = 0; i < Length; i++)
                yield return this[i];
        }

        /// <summary>
        ///     Gets the sum of the vector
        /// </summary>
        public double Sum()
        {
            double sum = 0;
            for (var i = 0; i < Length; i++)
                sum += this[i];
            return sum;
        }

        /// <summary>
        ///     Copies vector and return new vector copy
        /// </summary>
        public Vector Copy()
        {
            var v = new Vector(Length);
            for (var i = 0; i < Length; i++)
                v[i] = this[i];
            return v;
        }

        /// <summary>
        ///     Returs double array of the vector vclues
        /// </summary>
        public double[] ToArray()
        {
            return _vectorValues;
        }

        /// <summary>
        ///     Create matrix
        /// </summary>
        public Matrix ToMatrix()
        {
            return ToMatrix(VectorType.Row);
        }

        /// <summary>
        ///     Creates matrix according to type
        /// </summary>
        /// <param name="vectorType">Vector type</param>
        private Matrix ToMatrix(VectorType vectorType)
        {
            if (vectorType == VectorType.Row)
            {
                var m = new Matrix(1, Length);
                for (var j = 0; j < Length; j++)
                    m[0, j] = this[j];
                return m;
            }
            else
            {
                var m = new Matrix(Length, 1);
                for (var i = 0; i < Length; i++)
                    m[i, 0] = this[i];
                return m;
            }
        }
    }
}