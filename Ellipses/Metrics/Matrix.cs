﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ellipses.Definations;

namespace Ellipses.Metrics
{
    [Serializable]
    public class Matrix
    {
        private double[][] _matrix;

        private Matrix()
        {
        }

        /// <summary>
        ///     Create matrix n x n matrix
        /// </summary>
        /// <param name="n">size</param>
        public Matrix(int n) : this(n, n)
        {
        }

        /// <summary>
        ///     Create new n x d matrix
        /// </summary>
        /// <param name="n">rows</param>
        /// <param name="d">cols</param>
        public Matrix(int n, int d)
        {
            Rows = n;
            Cols = d;
            _matrix = new double[n][];
            for (var i = 0; i < n; i++)
            {
                _matrix[i] = new double[d];
                for (var j = 0; j < d; j++)
                    _matrix[i][j] = 0;
            }
        }

        /// <summary>
        ///     Create new matrix with prepopulated
        ///     vals
        /// </summary>
        /// <param name="m">initial matrix</param>
        public Matrix(double[,] m)
        {
            Rows = m.GetLength(0);
            Cols = m.GetLength(1);
            _matrix = new double[Rows][];
            for (var i = 0; i < Rows; i++)
            {
                _matrix[i] = new double[Cols];
                for (var j = 0; j < Cols; j++)
                    _matrix[i][j] = m[i, j];
            }
        }

        public Matrix(double[][] m)
        {
            Rows = m.GetLength(0);
            if (Rows > 0)
                Cols = m[0].Length;
            else
                throw new InvalidOperationException("Insufficient information to construct Matrix");
            _matrix = m;
        }

        public int Rows { get; private set; }

        public int Cols { get; private set; }

        /// <summary>
        ///     Accessor
        /// </summary>
        /// <param name="i">Row</param>
        /// <param name="j">Column</param>
        /// <returns></returns>
        public double this[int i, int j]
        {
            get { return _matrix[i][j]; }
            set { _matrix[i][j] = value; }
        }

        /// <summary>
        ///     Returns row vector specified at index i
        /// </summary>
        /// <param name="i">row index</param>
        /// <returns></returns>
        public Vector this[int i]
        {
            get { return this[i, VectorType.Row]; }
            set { this[i, VectorType.Row] = value; }
        }

        /// <summary>
        ///     returns col/row vector at index j
        /// </summary>
        /// <param name="j">Col/Row</param>
        /// <param name="t">Row or Column</param>
        /// <returns>Vector</returns>
        private Vector this[int i, VectorType t]
        {
            get
            {
                if (t == VectorType.Row)
                {
                    if (i >= Rows)
                        throw new IndexOutOfRangeException();
                    return new Vector(_matrix[i]);
                }
                if (i >= Cols)
                    throw new IndexOutOfRangeException();
                return new Vector(_matrix, i);
            }
            set
            {
                if (t == VectorType.Row)
                {
                    if (i >= Rows)
                        throw new IndexOutOfRangeException();
                    if (value.Length > Cols)
                        throw new InvalidOperationException(string.Format("Vector has lenght larger then {0}", Cols));
                    for (var k = 0; k < Cols; k++)
                        _matrix[i][k] = value[k];
                }
                else
                {
                    if (i >= Cols)
                        throw new IndexOutOfRangeException();
                    if (value.Length > Rows)
                        throw new InvalidOperationException(string.Format("Vector has lenght larger then {0}", Cols));
                    for (var k = 0; k < Rows; k++)
                        _matrix[k][i] = value[k];
                }
            }
        }

        public double this[Func<double, bool> f]
        {
            set
            {
                for (var i = 0; i < Rows; i++)
                    for (var j = 0; j < Cols; j++)
                        if (f(_matrix[i][j]))
                            this[i, j] = value;
            }
        }

        public Matrix this[Func<Vector, bool> f, VectorType t]
        {
            get
            {
                var count = 0;
                if (t == VectorType.Row)
                {
                    for (var i = 0; i < Rows; i++)
                        if (f(this[i, t]))
                            count++;
                    var m = new Matrix(count, Cols);
                    var j = -1;
                    for (var i = 0; i < Rows; i++)
                        if (f(this[i, t]))
                            m[++j, t] = this[i, t];
                    return m;
                }
                else
                {
                    for (var i = 0; i < Cols; i++)
                        if (f(this[i, t]))
                            count++;
                    var m = new Matrix(Rows, count);
                    var j = -1;
                    for (var i = 0; i < Cols; i++)
                        if (f(this[i, t]))
                            m[++j, t] = this[i, t];
                    return m;
                }
            }
        }

        /// <summary>
        ///     Returns read-only transpose (uses matrix reference
        ///     to save space)
        ///     It will throw an exception if there is an attempt
        ///     to write to the matrix
        /// </summary>
        public Matrix T
        {
            get { return new Matrix {_matrix = _matrix, Cols = Rows, Rows = Cols}; }
        }

        public Vector GetVector(int index, int from, int to, VectorType type)
        {
            var v = new double[to - from + 1];
            for (int i = from, j = 0; i < to + 1; i++, j++)
                v[j] = this[index, type][i];
            return new Vector(v);
        }

        public IEnumerable<Vector> GetRows()
        {
            for (var i = 0; i < Rows; i++)
                yield return this[i, VectorType.Row];
        }

        public IEnumerable<Vector> GetCols()
        {
            for (var i = 0; i < Cols; i++)
                yield return this[i, VectorType.Column];
        }

        public double[] GetFirst()
        {
            return _matrix[0];
        }

        public IEnumerable<double> Select(Func<double[], double> func)
        {
            return _matrix.Select(func);
        }

        /// <summary>
        ///     Connects matrix
        /// </summary>
        public Matrix ConnectMatrix(Matrix matrix)
        {
            var newMatrix = new Matrix(Rows, Cols + matrix.Cols);
            for (var i = 0; i < Rows; i++)
                for (var j = 0; j < Cols; j++)
                    newMatrix[i, j] = this[i, j];
            for (var i = 0; i < Rows; i++)
                for (var j = 0; j < matrix.Cols; j++)
                    newMatrix[i, Cols + j] = matrix[i, j];
            return newMatrix;
        }

        /// <summary>
        ///     Combines matrix
        /// </summary>
        public Matrix CombineMatrix(Matrix matrix)
        {
            var newMatrix = new Matrix(Rows*Rows, Cols + matrix.Cols);
            for (var i = 0; i < Rows; i++)
                for (var j = 0; j < Cols; j++)
                    newMatrix[i, j] = this[i, j];
            for (var i = 0; i < Rows; i++)
                for (var j = 0; j < matrix.Cols; j++)
                    newMatrix[i, Cols + j] = matrix[i, j];
            return newMatrix;
        }

        /// <summary>
        ///     Creates a index vector
        /// </summary>
        public static int[] Indices(int from, int to)
        {
            var vector = new int[to - from];
            for (var i = 0; i < vector.Length; i++)
                vector[i] = from++;
            return vector;
        }
    }
}