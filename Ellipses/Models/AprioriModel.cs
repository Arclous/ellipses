﻿using System;
using System.Collections.Generic;

namespace Ellipses.Models
{
    [Serializable]
    public class AprioriModel
    {
        public AprioriModel(Dictionary<double[], int> workingSet, List<Tuple<double[], double[], double>> associationSet,
            double support)
        {
            WorkingSet = workingSet;
            AssociationSet = associationSet;
            Support = support;
        }

        //Data set as double array list 
        public Dictionary<double[], int> WorkingSet { get; private set; }

        //Associations
        public List<Tuple<double[], double[], double>> AssociationSet { get; private set; }

        //Support percent
        public double Support { get; private set; }
    }
}