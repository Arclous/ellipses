﻿namespace Ellipses.Models
{
    public class ElbowResult
    {
        public double[] Points { get; set; }
        public int Clusters { get; set; }
    }
}