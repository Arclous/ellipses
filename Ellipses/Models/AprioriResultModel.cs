﻿using System;
using System.Collections.Generic;

namespace Ellipses.Models
{
    public class AprioriResultModel
    {
        public AprioriResultModel(Dictionary<double[], int> workingSet,
            List<Tuple<double[], double[], double>> associationSet, List<Tuple<double[], double[], double>> exactSet, Dictionary<double[], int> count)
        {
            WorkingSet = workingSet;
            AssociationSet = associationSet;
            ExactSet = exactSet;
            Count = count;
        }

        //Data set as double array list 
        public Dictionary<double[], int> WorkingSet { get;  set; }

        //Associations
        public List<Tuple<double[], double[], double>> AssociationSet { get;  set; }

        public List<Tuple<double[], double[], double>> ExactSet { get; set; }

        public Dictionary<double[], int> Count { get; set; }
    }
}