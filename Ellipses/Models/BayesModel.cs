﻿using System;
using System.Collections.Generic;
using Ellipses.Metrics;

namespace Ellipses.Models
{
    [Serializable]
    public class BayesModel
    {
        public List<NaiveBayesProbability> FeatureProbabilities;
        public Matrix LabelMatrix;
        public Dictionary<double, double> LabelProbabilities;

        public BayesModel(Dictionary<double, double> labelProbabilities, List<NaiveBayesProbability> featureProbabilities,
           Matrix labelMatrix)
        {
            FeatureProbabilities = featureProbabilities;
            LabelMatrix = labelMatrix;
            LabelProbabilities = labelProbabilities;
        }
    }
}