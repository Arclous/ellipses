﻿namespace Ellipses.Models
{
    public class NaiveBayesFeature
    {
        public string FeatureName { get; set; }
        public object FeatureValue { get; set; }
    }
}