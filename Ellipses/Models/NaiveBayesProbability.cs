﻿using System;

namespace Ellipses.Models
{

    [Serializable]
    public class NaiveBayesProbability
    {
        public int FeatureIndex { get; set; }
        public double Feature { get; set; }
        public double Label { get; set; }
        public double FeatureProbability { get; set; }

        public int FeatureTotal { get; set; }
 
    }
}