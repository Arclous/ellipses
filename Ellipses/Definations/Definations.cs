﻿namespace Ellipses.Definations
{
    public enum ElbowOrigin
    {
        Datapoints = 0,
        Centroids = 1
    }

    public enum VectorType
    {
        Row,
        Column
    }

    public enum StringType
    {
        Character,
        Word
    }
}