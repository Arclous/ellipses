﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellipses.Calculaters;
using Ellipses.Helpers;
using Ellipses.Interfaces;
using Ellipses.Metrics;

namespace Ellipses.Algorithms
{
    /// ******************Description************************************************************************************************************************************************
    /// KNearestNeighbour class is the helper for finding nearest neighbour on the dataset which can be build by
    /// given data and its features. Given data can be provided as custom object type,the fields which is/are going to be use as feature for processing
    /// should be marked as AiFieldAttribute, other fields which is/are not marked with as AiFieldAttribute will not be processing.
    /// There are three distance calculation formulas which can be used for distance calculation Euclidean,Manhattan and Minkowski. It is possible to overwrite or create different 
    /// distance calculater by inheritancing IDistanceCalculater interface and passing new distance calculater on the KNearestNeighbour constructer or GetNearestNeighbours function.
    /// Default distance calculater is Euclidean.
    /// *****************************************************************************************************************************************************************************
    public class KNearestNeighbour<TObjectType> : IKNearestNeighbour
    {
        //Helper class for converting models
        private readonly IConverter _converter;

        //Helper classes for calculating distance, normalization and converting models
        private readonly IDistanceCalculater _distanceCalculater;

        private readonly INormalizer _normalizer;

        //Flag if the data normalized or not
        private bool _dataNormalized;

        //Data set as double array list 
        private double[][] _dataSet;


        //Data set as matrix
        private Matrix _matrix;

        //Data set without convertation as base shape
        private IList _models;

        /// <summary>
        ///     KNearestNeighbour
        /// </summary>
        /// <param name="distanceCalculater">Distance calculater</param>
        /// <param name="normalizer">Normalizer</param>
        /// <param name="converter">Converter for the models</param>
        public KNearestNeighbour(IDistanceCalculater distanceCalculater = null, INormalizer normalizer = null,
            IConverter converter = null)
        {
            _distanceCalculater = distanceCalculater ?? new Euclidean();
            _normalizer = normalizer ?? new Normalizer();
            _converter = converter ?? new Converter();
        }

        /// <summary>
        ///     Load data set
        /// </summary>
        /// <param name="models">Data set</param>
        /// <param name="normalization">Normalize data</param>
        public void LoadDataSet<T>(T[] models, bool normalization = false)
        {
            _dataNormalized = normalization;
            _dataSet = _converter.ConvertModels(models);
            _models = models;
            if (normalization)
                _dataSet = _normalizer.Normalize(_dataSet);

            _matrix = new Matrix(_dataSet);
        }

        /// <summary>
        ///     Get Nearest Neighbours from the data set according to given data by y
        /// </summary>
        /// <param name="y">Data to check</param>
        /// <param name="k">Neighbours</param>
        /// <param name="substractOrigin">Return data with origin</param>
        /// <param name="distanceCalculater">Distance calculater</param>
        public IList GetNearestNeighbours(double[] y, int k,
            bool substractOrigin = false, IDistanceCalculater distanceCalculater = null)
        {
            var distanceHelper = distanceCalculater ?? _distanceCalculater;
            var dists = new Dictionary<TObjectType, double>();
            var data = _matrix;
            var featureLength = y.Length - 1;

            var normalizedY = y;
            if (_dataNormalized)
                normalizedY = _normalizer.NormalizeInput(y);

            var inputVector = new Vector(normalizedY);
            for (var i = 0; i <= data.Rows - 1; i++)
            {
                var distance = 0d;
                var x = data[i];
                distance = distanceHelper.CalculateDistance(x, inputVector, featureLength);
                dists.Add((TObjectType)_models[i], distance);
            }
            var sorted = dists.OrderBy(kp => kp.Value);

            return substractOrigin ? sorted.Skip(1).Take(k).ToArray() : sorted.Take(k).ToArray();
        }
    }
}
