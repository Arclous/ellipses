﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System.Collections.Generic;
using System.Linq;
using Ellipses.Helpers;
using Ellipses.Interfaces;
using Ellipses.Metrics;
using Ellipses.Models;

namespace Ellipses.Algorithms
{
    public class NaiveBayes : INaiveBayes
    {
        //Helper class for converting models
        private readonly IConverter _converter;

        //Vector helper for the various operations
        private readonly IHelper _helper;

        //Normalizer
        private readonly INormalizer _normalizer;

        //Data set as double array list 
        private double[][] _dataSet;

        //Probabilities of the features
        private List<NaiveBayesProbability> _featureProbabilities;

        //Labels as matrix
        private Matrix _labelMatrix;

        //Probabilities of the labels
        private Dictionary<double, double> _labelProbabilities;

        //Data set as matrix
        private Matrix _matrix;

        /// <summary>
        ///     Naive Bayes
        /// </summary>
        /// <param name="normalizer">Normalizer</param>
        /// <param name="converter">Converter for the models</param>
        public NaiveBayes(INormalizer normalizer = null, IConverter converter = null)
        {
            _normalizer = normalizer ?? new Normalizer();
            _converter = converter ?? new Converter();
            _helper = new Helper();
        }

        /// <summary>
        ///     Load data set
        /// </summary>
        /// <param name="models">Data set</param>
        /// <param name="normalization">Normalize data</param>
        public void LoadDataSet<T>(T[] models, bool normalization = false)
        {
            var dimensionalProcess = _converter.IsDimensionalFieldExist(models);
            var newLabelList = new List<double[]>();
            var dimensionalMatrix = new Matrix(0);
            _dataSet = _converter.ConvertModels(models);

            if (dimensionalProcess)
            {
                var dimensionalData = _converter.ConvertDimensionalModels(models);
                if (normalization)
                    dimensionalData = _normalizer.Normalize(dimensionalData);

                dimensionalMatrix = new Matrix(dimensionalData);
            }

            if (normalization)
                _dataSet = _normalizer.Normalize(_dataSet);

            _matrix = new Matrix(_dataSet);
            _labelMatrix = _converter.ConvertLabelsAsMatrix(models);

            if (dimensionalProcess)
            {
                var orjMatrixValues = new List<double[]>();
                var dimensionalMatrixValues = new List<double[]>();
                var newMatrixValues = new List<double[]>();

                for (var dimensionalRow = 0; dimensionalRow < dimensionalMatrix.Rows; dimensionalRow++)
                    dimensionalMatrixValues.Add(dimensionalMatrix[dimensionalRow].ToArray());

                for (var matrixRow = 0; matrixRow < _matrix.Rows; matrixRow++)
                    orjMatrixValues.Add(_matrix[matrixRow].ToArray());

                for (var i = 0; i < dimensionalMatrixValues.Count; i++)
                {
                    for (var matrixRow = 0; matrixRow < _matrix.Rows; matrixRow++)
                    {
                        newMatrixValues.Add(orjMatrixValues[i].Concat(dimensionalMatrixValues[matrixRow]).ToArray());
                        newLabelList.Add(_labelMatrix[i].ToArray());
                    }
                }

                _matrix = new Matrix(newMatrixValues.ToArray());
                _labelMatrix = new Matrix(newLabelList.ToArray());
            }
        }

        /// <summary>
        ///     Trains model for prediction
        /// </summary>
        public INaiveBayesPredicter Train()
        {
            //Calculate probabilities of the labels
            CalculateProbabilityOfLabels(ref _labelMatrix);

            //Calculate conditional probabilities of the features
            CalculateProbabilityOfFeatures(ref _matrix, ref _labelMatrix);

            //Preapre and return trained model for prediction
            var naiveBayesPredicter = new NaiveBayesPredicter(_labelProbabilities, _featureProbabilities, _labelMatrix);
            return naiveBayesPredicter;
        }

        /// <summary>
        ///     Saves bayes image classifier trained model
        /// </summary>
        /// <param name="fileName">Filename to save trained model</param>
        public void SaveModel(string fileName)
        {
            var bayesTrainedModel = new BayesModel(_labelProbabilities, _featureProbabilities, _labelMatrix);
            _helper.SaveModel(fileName, bayesTrainedModel);
        }

        /// <summary>
        ///     Loades trained models
        /// </summary>
        /// <param name="fileName">Filename of the trained model to load</param>
        public INaiveBayesPredicter LoadModel(string fileName)
        {
            var trainedModel = _helper.LoadModel<BayesModel>(fileName);
            _labelProbabilities = trainedModel.LabelProbabilities;
            _featureProbabilities = trainedModel.FeatureProbabilities;
            _labelMatrix = trainedModel.LabelMatrix;
            var naiveBayesPredicter = new NaiveBayesPredicter(_labelProbabilities, _featureProbabilities, _labelMatrix);
            return naiveBayesPredicter;
        }

        #region Helpers
        private void CalculateProbabilityOfLabels(ref Matrix labelMatrix)
        {
            _labelProbabilities = new Dictionary<double, double>();
            var labelList = labelMatrix.GetRows().Select(x => x[0]).Distinct().ToList();
            for (var i = 0; i < labelList.Count(); i++)
            {
                var lbl = labelList[i];
                var tLbl = labelMatrix.GetRows().Where(x => x[0] == lbl).ToList().Count();
                var pValue = (double) tLbl/labelMatrix.Rows;
                _labelProbabilities.Add(lbl, pValue);
            }
        }

        private void CalculateProbabilityOfFeatures(ref Matrix matrix, ref Matrix labelMatrix)
        {
            _featureProbabilities = new List<NaiveBayesProbability>();

            //Connect matrix
            var connectedMatrix = matrix.ConnectMatrix(labelMatrix);
            var labelList = labelMatrix.GetRows().Select(x => x[0]).Distinct().ToList();
            for (var j = 0; j < labelList.Count; j++)
            {
                for (var i = 0; i < matrix.Cols; i++)
                {
                    var featureVal = matrix.Select(x => x[i]).Distinct().ToList();
                    for (var t = 0; t < featureVal.Count; t++)
                    {
                        var tFeature =
                            connectedMatrix.GetRows()
                                           .Count(x => x[i] == featureVal[t] && x[connectedMatrix.Cols - 1] == j);
                        var pFeature = (double) tFeature/labelMatrix.GetRows().Count(c => c[0] == j);
                        var probability = new NaiveBayesProbability
                        {
                            FeatureIndex = i,
                            FeatureProbability = pFeature,
                            Feature = featureVal[t],
                            FeatureTotal = tFeature,
                            Label = j
                        };
                        _featureProbabilities.Add(probability);
                    }
                }
            }
        }
        #endregion
    }
}