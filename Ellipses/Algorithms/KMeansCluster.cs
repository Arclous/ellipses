﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

/*
 The implemantation below changed and improved according to requirements if BlueAi, the base of flowing implemantation belongs to Sverrir Sigmundarson
 * for more information https://blog.sverrirs.com/2013/08/simple-k-means-algorithm-in-c.html
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ellipses.Calculaters;
using Ellipses.Definations;
using Ellipses.Helpers;
using Ellipses.Helpers.Attributes;
using Ellipses.Interfaces;
using Ellipses.Metrics;
using Ellipses.Models;

namespace Ellipses.Algorithms
{
    public class KMeansCluster : IKMeansCluster
    {
        //Helper class for converting models
        private readonly IConverter _converter;

        //Helper classes for calculating distance, normalization and converting models
        private readonly IDistanceCalculater _distanceCalculater;

        //Vector helper for the various operations
        private readonly IHelper _helper;

        //Normalizer
        private readonly INormalizer _normalizer;

        //Data set as double array list 
        private double[][] _dataSet;

        //Data set as matrix
        private Matrix _matrix;

        //Data set without convertation as base shape
        private IList _models;

        /// <summary>
        ///     KMeansCluster
        /// </summary>
        /// <param name="distanceCalculater">Distance calculater</param>
        /// <param name="normalizer">Normalizer</param>
        /// <param name="converter">Converter for the models</param>
        public KMeansCluster(IDistanceCalculater distanceCalculater = null, INormalizer normalizer = null,
            IConverter converter = null)
        {
            _distanceCalculater = distanceCalculater ?? new Euclidean();
            _normalizer = normalizer ?? new Normalizer();
            _converter = converter ?? new Converter();
            _helper = new Helper();
        }

        /// <summary>
        ///     Load data set
        /// </summary>
        /// <param name="models">Data set</param>
        /// <param name="normalization">Normalize data</param>
        public void LoadDataSet<T>(T[] models, bool normalization = false)
        {
            _dataSet = _converter.ConvertModels(models);
            _models = models;
            if (normalization)
                _dataSet = _normalizer.Normalize(_dataSet);

            _matrix = new Matrix(_dataSet);
        }


        /// <summary>
        ///     Clusters the given item set into the desired number of clusters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// or a class struct that exposes properties using the
        /// <see cref="AiFieldAttribute" />
        /// <param name="clusterCount">the desired number of clusters</param>
        /// <param name="maxIterations">the maximum number of iterations to perform</param>
        /// <param name="randomSeed">
        ///     optional, a seed for the random generator that initially arranges the clustering of the nodes
        ///     (specify the same value to ensure that the start ordering will be the same)
        /// </param>
        /// <param name="initialCentroidIndices">
        ///     optional, the initial centroid configuration (as indicies into the <see cref="_dataSet" /> array). When this is
        ///     used the <see cref="randomSeed" /> has no effect.
        ///     Experiment with this as the initial arrangements of the centroids has a huge impact on the final cluster
        ///     arrangement.
        /// </param>
        /// <returns>
        ///     a result containing the items arranged into clusters as well as the centroids converged on and the total
        ///     distance value for the cluster nodes.
        /// </returns>
        public KMeansResults<T> Cluster<T>(int clusterCount, int maxIterations, int randomSeed = 0,
            int[] initialCentroidIndices = null)
        {
            var hasChanges = true;
            var iteration = 0;
            double totalDistance = 0;
            var numData = _matrix.Rows;
            var numAttributes = _matrix[0].Length;

            var clustering = _helper.InitializeClustering(numData, clusterCount, randomSeed);

            // Create cluster means and centroids
            var means = _helper.BuildMatrix(clusterCount, numAttributes);
            var centroidIdx = new int[clusterCount];
            var clusterItemCount = new int[clusterCount];

            // If we specify initial centroid indices then let's assign clustering based on those immediately
            if (initialCentroidIndices != null && initialCentroidIndices.Length == clusterCount)
            {
                centroidIdx = initialCentroidIndices;
                AssignClustering(_dataSet, clustering, centroidIdx, clusterCount, numAttributes - 1);
            }

            // Perform the clustering
            while (hasChanges && iteration < maxIterations)
            {
                clusterItemCount = new int[clusterCount];
                totalDistance = CalculateClustering(_matrix, clustering, ref means, ref centroidIdx,
                    clusterCount, ref clusterItemCount, numAttributes - 1);

                hasChanges = AssignClustering(_dataSet, clustering, centroidIdx, clusterCount, numAttributes - 1);
                ++iteration;
            }

            // Create the final clusters
            var clusters = new T[clusterCount][];
            for (var k = 0; k < clusters.Length; k++)
                clusters[k] = new T[clusterItemCount[k]];

            var clustersCurIdx = new int[clusterCount];
            for (var i = 0; i < clustering.Length; i++)
            {
                clusters[clustering[i]][clustersCurIdx[clustering[i]]] = ((IList<T>) _models)[i];
                ++clustersCurIdx[clustering[i]];
            }

            // Return the results
            return new KMeansResults<T>(clusters, means, centroidIdx, totalDistance);
        }

        /// <summary>
        ///     Organize clusters with labels
        /// </summary>
        /// <param name="result">Cluster result referance</param>
        /// <param name="labels">Labels</param>
        /// <param name="order">Order, as default x=>x</param>
        public Dictionary<string, List<T>> OrganizeClustersWithLabel<T>(ref KMeansResults<T> result, string[] labels,
            Func<int, int> order = null)
        {
            return _helper.OrganizeClustersWithLabel(ref result, labels, order);
        }

        /// <summary>
        ///     Apply Elbow to find best k
        /// </summary>
        /// <param name="origin">Origin for applying elbow</param>
        /// <param name="max">
        ///     Max cluster number to calculate,default it is 0,means total cluster number is the total data point
        ///     number
        /// </param>
        /// <param name="maxIterations">the maximum number of iterations to perform</param>
        public ElbowResult ApplyElbow<T>(ElbowOrigin origin, int max = 0, int maxIterations = 10)
        {
            if (max == 0) max = _dataSet.Length - 1;
            return origin == ElbowOrigin.Datapoints
                ? ApplyElbow<T>(max, maxIterations)
                : ApplyElbowCentroids<T>(max, maxIterations);
        }

        /// <summary>
        ///     Apply Elbow to find best k by using data points
        /// </summary>
        /// <param name="max">Max cluster number to calculate</param>
        /// <param name="maxIterations">the maximum number of iterations to perform</param>
        private ElbowResult ApplyElbow<T>(int max, int maxIterations = 10)
        {
            var elbowResult = new ElbowResult
            {
                Points = new double[max],
                Clusters = -1
            };

            double sum = 0;
            for (var i = 1; i <= max - 1; i++)
            {
                var cls = Cluster<T>(i, maxIterations);
                for (var k = 0; k < cls.Clusters.Length; k++)
                {
                    for (var t = 0; t < cls.Means.Rows; t++)
                    for (var e = 0; e < cls.Means[0].Length; e++)
                        sum += _dataSet.Sum(t1 => Math.Pow(t1[e] - cls.Means[t][e], 2));

                    elbowResult.Points[i] = sum;


                    if (elbowResult.Clusters == -1)
                        if (i != 0)
                            if (elbowResult.Points[i - 1] * 2 > elbowResult.Points[i])
                                elbowResult.Clusters = i;
                }
            }

            return elbowResult;
        }

        /// <summary>
        ///     Apply Elbow to find best k by using data centroids
        /// </summary>
        /// <param name="max">Max cluster number to calculate</param>
        /// <param name="maxIterations">the maximum number of iterations to perform</param>
        private ElbowResult ApplyElbowCentroids<T>(int max, int maxIterations = 10)
        {
            var elbowResult = new ElbowResult
            {
                Points = new double[max],
                Clusters = -1
            };


            double sum = 0;
            for (var i = 1; i <= max - 1; i++)
            {
                var cls = Cluster<T>(i, maxIterations);
                for (var j = 0; j <= cls.Centroids.Length - 1; j++)
                for (var t = 0; t <= cls.Means[j].Length - 1; t++)
                    sum += Math.Pow(cls.Centroids[j] - cls.Means[j][t], 2);

                elbowResult.Points[i] = sum;

                if (elbowResult.Clusters == -1)
                    if (i != 0)
                        if (elbowResult.Points[i - 1] * 2 > elbowResult.Points[i])
                            elbowResult.Clusters = i - 1;
            }
            return elbowResult;
        }

        private double CalculateClustering(Matrix data, IReadOnlyList<int> clustering,
            ref Matrix means,
            ref int[] centroidIdx,
            int clusterCount, ref int[] clusterItemCount, int length)
        {
            // Reset the means to zero for all clusters
            foreach (var mean in means.GetRows())
                for (var i = 0; i < mean.Length; i++)
                    mean[i] = 0;

            // Calculate the means for each cluster
            // Do this in two phases, first sum them all up and then divide by the count in each cluster
            for (var i = 0; i < data.Rows; i++)
            {
                // Sum up the means
                var row = data[i];
                var clusterIdx = clustering[i]; // What cluster is data i assigned to
                ++clusterItemCount[clusterIdx]; // Increment the count of the cluster that row i is assigned to
                for (var j = 0; j < row.Length; j++)
                    means[clusterIdx][j] += row[j];
            }

            // Now divide to get the average
            for (var k = 0; k < means.Rows; k++)
            for (var a = 0; a < means[k].Length; a++)
            {
                var itemCount = clusterItemCount[k];
                means[k][a] /= itemCount > 0 ? itemCount : 1;
            }

            double totalDistance = 0;
            // Calc the centroids
            var minDistances = new double[clusterCount].Select(x => double.MaxValue).ToArray();
            for (var i = 0; i < data.Rows; i++)
            {
                var clusterIdx = clustering[i];
                var distance = _distanceCalculater.CalculateDistance(data[i], means[clusterIdx], length);
                totalDistance += distance;
                if (distance < minDistances[clusterIdx])
                {
                    minDistances[clusterIdx] = distance;
                    centroidIdx[clusterIdx] = i;
                }
            }
            //double totalCentroidDistance = minDistances.Sum();

            return totalDistance;
        }


        /// <summary>
        ///     Calculates the distance for each point in <see cref="data" /> from each of the centroid in
        ///     <see cref="centroidIdx" /> and
        ///     assigns the data item to the cluster with the minimum distance.
        /// </summary>
        /// <returns>true if any clustering arrangement has changed, false if clustering did not change.</returns>
        private bool AssignClustering(IReadOnlyList<double[]> data, IList<int> clustering,
            IReadOnlyList<int> centroidIdx, int clusterCount,
            int length)
        {
            var changed = false;

            for (var i = 0; i < data.Count; i++)
            {
                var minDistance = double.MaxValue;
                var minClusterIndex = -1;

                for (var k = 0; k < clusterCount; k++)
                {
                    var distance = _distanceCalculater.CalculateDistance(data[i], data[centroidIdx[k]], length);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        minClusterIndex = k;
                    }
                }
                // Re-arrange the clustering for datapoint if needed
                if (minClusterIndex != -1 && clustering[i] != minClusterIndex)
                {
                    changed = true;
                    clustering[i] = minClusterIndex;
                }
            }

            return changed;
        }
    }
}