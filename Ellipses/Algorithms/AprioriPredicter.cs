﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Ellipses.Interfaces;
using Ellipses.Models;

namespace Ellipses.Algorithms
{
    internal class AprioriPredicter : IAprioriPredicter
    {
        /// <summary>
        ///     Naive Bayes
        /// </summary>
        /// <param name="workingSet">Trained working set</param>
        /// <param name="associationSet">Calculated association Set</param>
        /// p
        public AprioriPredicter(Dictionary<double[], int> workingSet,
            List<Tuple<double[], double[], double>> associationSet)
        {
            WorkingSet = workingSet;
            AssociationSet = associationSet;
        }

        //Data set as double array list 
        private Dictionary<double[], int> WorkingSet { get; }

        //Associations
        private List<Tuple<double[], double[], double>> AssociationSet { get; }

        /// <summary>
        ///     Naive Bayes
        /// </summary>
        /// <param name="val">Value for prediction</param>
        public AprioriResultModel Predict(double[] val)
        {
            return new AprioriResultModel(GetAnalyzedWorkingSet(val), GetAnalyzedAccociationsSet(val),
                GetExcatAssociationSet(val), GetCountDictionary(val));
        }

        private List<Tuple<double[], double[], double>> GetAnalyzedAccociationsSet(double[] val)
        {
            var resultassociationSet = new List<Tuple<double[], double[], double>>();

            for (var i = 0; i < AssociationSet.Count; i++)
            {
                for (var j = 0; j < val.Length; j++)
                {
                    var itedm = AssociationSet.Where(x => x.Item1.Contains(val[j]));
                    foreach (
                        var v in itedm.Where(v => resultassociationSet.FirstOrDefault(x => x.Item1 == v.Item1) == null))
                        resultassociationSet.Add(v);
                }
            }

            return resultassociationSet;
        }

        private Dictionary<double[], int> GetAnalyzedWorkingSet(double[] val)
        {
            var resultWorkingSet = new Dictionary<double[], int>();
            for (var i = 0; i < WorkingSet.Count; i++)
            {
                for (var j = 0; j < val.Length; j++)
                {
                    var itedm = WorkingSet.Where(x => x.Key.Contains(val[j]));
                    foreach (var v in itedm.Where(v => !resultWorkingSet.ContainsKey(v.Key)))
                        resultWorkingSet.Add(v.Key, v.Value);
                }
            }
            return resultWorkingSet;
        }

        private Dictionary<double[], int> GetCountDictionary(IEnumerable<double> val)
        {
            var count = WorkingSet.FirstOrDefault(x => x.Key.SequenceEqual(val));
            var dic = new Dictionary<double[], int>();
            if (count.Key != null)
                dic.Add(count.Key, count.Value);

            return dic;
        }

        private List<Tuple<double[], double[], double>> GetExcatAssociationSet(IEnumerable<double> val)
        {
            var exact = AssociationSet.Where(t => t.Item1.SequenceEqual(val)).ToList();
            return exact;
        }
    }
}