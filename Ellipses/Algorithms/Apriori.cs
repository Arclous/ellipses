﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Ellipses.Helpers;
using Ellipses.Interfaces;
using Ellipses.Models;

namespace Ellipses.Algorithms
{
    public class Apriori : IApriori
    {
        //Helper class for converting models
        private readonly IConverter _converter;

        //Vector helper for the various operations
        private readonly IHelper _helper;

        //Associations
        private List<Tuple<double[], double[], double>> _associationSet = new List<Tuple<double[], double[], double>>();

        //Data set as double array list 
        private double[][] _dataSet;

        //Pairs for frequency
        private Dictionary<double[], int> _pairs;

        //Pairs for frequency
        private Dictionary<double[], int> _pairsN2;

        //Support percent
        private double _support;

        //Support item
        private int _supportEach;

        //Data set as double array list 
        private Dictionary<double[], int> _workingSet = new Dictionary<double[], int>();

        /// <summary>
        ///     Naive Bayes
        /// </summary>
        /// <param name="support">Support Percent</param>
        /// p
        /// <param name="converter">Converter for the models</param>
        public Apriori(double support = 60, IConverter converter = null)
        {
            _converter = converter ?? new Converter();
            _support = support;
            _helper = new Helper();
        }

        /// <summary>
        ///     Load data set
        /// </summary>
        /// <param name="models">Data set</param>
        public void LoadDataSet<T>(T[] models)
        {
            var dimensionalProcess = _converter.IsDimensionalFieldExist(models);
            _dataSet = _converter.ConvertModels(models);

            if (dimensionalProcess)
                _dataSet = _converter.ConvertDimensionalModels(models);

            //find the key limit for the frequency
            _supportEach = Convert.ToInt32((_dataSet.Count())*(_support/100));
        }

        /// <summary>
        ///     Trains model for prediction
        /// </summary>
        public IAprioriPredicter Train()
        {
            //Organize items for L1
            OrganizeItems(_dataSet);

            //Calculate totals
            CalculateTotals(_workingSet);

            //Filter according to limit
            FilterTotal();

            //Make Pairs
            MakeN1Pair(_workingSet.Keys.ToArray());

            //Calculate Pairs Frequency
            CalculatePairFrequency(_pairs);

            //Filter according to limit
            FilterPairs();

            MakeN2Pairs(_pairs.Keys.ToArray());

            //Combine Sets
            _workingSet = _workingSet.Concat(_pairs).ToDictionary(x => x.Key, x => x.Value);
            _workingSet = _workingSet.Concat(_pairsN2).ToDictionary(x => x.Key, x => x.Value);

            //Calculate n2s
            CalculateTotals(_pairsN2);

            var aprioriPredicter = new AprioriPredicter(_workingSet, _associationSet);
            return aprioriPredicter;
        }

        /// <summary>
        ///     Saves bayes image classifier trained model
        /// </summary>
        /// <param name="fileName">Filename to save trained model</param>
        public void SaveModel(string fileName)
        {
            var aprioriModel = new AprioriModel(_workingSet, _associationSet, _support);
            _helper.SaveModel(fileName, aprioriModel);
        }

        /// <summary>
        ///     Loades trained models
        /// </summary>
        /// <param name="fileName">Filename of the trained model to load</param>
        public IAprioriPredicter LoadModel(string fileName)
        {
            var trainedModel = _helper.LoadModel<AprioriModel>(fileName);
            _workingSet = trainedModel.WorkingSet;
            _associationSet = trainedModel.AssociationSet;
            _support = trainedModel.Support;
            var aprioriPredicter = new AprioriPredicter(_workingSet, _associationSet);
            return aprioriPredicter;
        }

        #region Helpers
        /// <summary>
        ///     Organize N1 items
        /// </summary>
        private void OrganizeItems(IReadOnlyList<double[]> items)
        {
            for (var i = 0; i < items.Count(); i++)
            {
                for (var j = 0; j < items[i].Length; j++)
                {
                    var key = new List<double> {items[i][j]}.ToArray();
                    if (!IsPairExist(_workingSet.Keys.ToArray(), key))
                        _workingSet.Add(key, 0);
                }
            }
        }

        /// <summary>
        ///     Filters according to limit
        /// </summary>
        private void FilterTotal()
        {
            _workingSet = _workingSet.Where(x => x.Value >= _supportEach).ToDictionary(v => v.Key, v => v.Value);
        }

        /// <summary>
        ///     Filters according to limit
        /// </summary>
        private void FilterPairs()
        {
            _pairs = _pairs.Where(x => x.Value >= _supportEach).ToDictionary(v => v.Key, v => v.Value);
        }

        /// <summary>
        ///     Makes pairs
        /// </summary>
        /// <param name="items">Items for making N1 pairs</param>
        private void MakeN1Pair(IEnumerable<double[]> items)
        {
            _pairs = new Dictionary<double[], int>();
            foreach (var pair in items.Select(GetN1Pairs))
            {
                for (var j = 0; j < pair.Length; j++)
                {
                    if (!IsPairExist(_pairs.Keys.ToArray(), pair[j]))
                    {
                        _pairs.Add(pair[j], 0);
                    }
                }
            }
        }

        /// <summary>
        ///     Makes pairs
        /// </summary>
        /// <param name="items">Items for making N2 pairs</param>
        private void MakeN2Pairs(IEnumerable<double[]> items)
        {
            _pairsN2 = new Dictionary<double[], int>();
            var pairsTemp = _pairs.ToDictionary(x => x.Key, x => x.Value);
            foreach (var item in items)
            {
                var otherItems = pairsTemp.Where(x => x.Key != item).ToList();
                var isSelfExist = otherItems.Where(x => x.Key.Contains(item[0])).ToList();

                if (isSelfExist.Count == 0)
                    pairsTemp.Remove(item);
            }

            foreach (var pair in pairsTemp.Select(item => GetN2Pairs(pairsTemp, item.Key)))
            {
                for (var j = 0; j < pair.Length; j++)
                {
                    if (!IsPairExist(_pairsN2.Keys.ToArray(), pair[j]))
                        _pairsN2.Add(pair[j], 0);
                }
            }

            foreach (var assc in _pairsN2)
                CalculateAssociations(assc.Key);
        }

        /// <summary>
        ///     Calculates totals
        /// </summary>
        private void CalculateTotals(Dictionary<double[], int> items)
        {
            var list = items.ToList();
            for (var i = 0; i < list.Count(); i++)
            {
                var key = list[i].Key;
                _workingSet[key] = GetCount(key);
            }
        }

        /// <summary>
        ///     Calculate Pairs Frequency
        /// </summary>
        private void CalculatePairFrequency(Dictionary<double[], int> items)
        {
            var list = items.ToList();
            for (var i = 0; i < list.Count(); i++)
            {
                var key = list[i].Key;
                _pairs[key] = GetCount(key);
            }
        }

        /// <summary>
        ///     Gets pairs accroding to key and set count
        /// </summary>
        /// <param name="key">Key to get pairs</param>
        private void CalculateAssociations(IEnumerable<double> key)
        {
            var n1List = key.Select(t => new[] {t}).ToList();
            var n2List = new List<double[]>();
            for (var i = 0; i < n1List.Count; i++)
            {
                var otherItems = n1List.Where(x => x != n1List[i]).ToList();
                for (var j = 0; j < otherItems.Count; j++)
                {
                    var newPair = n1List[i].Concat(otherItems[j]).ToArray();
                    if (!IsPairExist(n2List, newPair))
                        n2List.Add(newPair);
                }
            }

            foreach (var tup in from n1Item in n1List
                                let set1 = GetAssociationPair(n2List, n1Item)
                                select
                                    new Tuple<double[], double[], double>(n1Item, set1[0],
                                        CalculateAssociation(n1Item, set1[0])))
            {
                _associationSet.Add(tup);
            }
            foreach (var n2Item in n2List)
            {
                var set1 = GetAssociationPair(n2List, n2Item);
                var setLis = new List<double> {GetRepeatedItem(set1)};

                var tup = new Tuple<double[], double[], double>(n2Item, setLis.ToArray(),
                    CalculateAssociation(n2Item, set1[0]));
                _associationSet.Add(tup);
            }
        }

        /// <summary>
        ///     Calculates association
        /// </summary>
        /// <param name="val1">Value 1</param>
        /// ///
        /// <param name="val2">Value 2</param>
        private double CalculateAssociation(IReadOnlyList<double> val1, IReadOnlyList<double> val2)
        {
            double total1 = GetCount(val1);
            double total2 = GetCommonTransactions(GetTransactions(val1), val2).Count();
            return total2/total1*100;
        }

        /// <summary>
        ///     Finds most repeated item
        /// </summary>
        /// <param name="val1">List to search</param>
        private static double GetRepeatedItem(IReadOnlyList<double[]> val1)
        {
            var totalList = new Dictionary<double, int>();
            for (var i = 0; i < val1.Count(); i++)
            {
                for (var j = 0; j < val1.Count; j++)
                {
                    if (!totalList.ContainsKey(val1[i][j]))
                        totalList.Add(val1[i][j], 0);
                    else
                        totalList[val1[i][j]] += 1;
                }
            }

            var max = totalList.Where(x => x.Value == totalList.Values.Max()).ToDictionary(x => x.Key, x => x.Value);
            return max.Keys.FirstOrDefault();
        }

        /// <summary>
        ///     Gets Association Pair
        /// </summary>
        /// <param name="list">List to search</param>
        /// <param name="value">Value to search</param>
        private static double[][] GetAssociationPair(IReadOnlyList<double[]> list, double[] value)
        {
            var pairs = new List<double[]>();
            for (var i = 0; i < list.Count(); i++)
            {
                var itm = list[i];
                for (var j = 0; j < value.Length; j++)
                {
                    var bres = new bool[value.Length];
                    if (!itm.Contains(value[j]))
                        bres[j] = true;

                    if (bres.Any(x => x))
                        pairs.Add(itm);
                }
            }

            return pairs.ToArray();
        }

        /// <summary>
        ///     Gets pairs accroding to key and set count
        /// </summary>
        /// <param name="key">Key to get pairs</param>
        private double[][] GetN1Pairs(double[] key)
        {
            var otherItems = _workingSet.Where(x => x.Key != key).ToList();
            return otherItems.Select(t => key.Concat(t.Key).ToArray()).ToArray();
        }

        /// <summary>
        ///     Gets pairs accroding to key and set count
        /// </summary>
        /// <param name="key">Key to get pairs</param>
        /// <param name="items">Items for getting pairs</param>
        private static double[][] GetN2Pairs(Dictionary<double[], int> items, double[] key)
        {
            var otherItems =
                items.Where(x => x.Key.OrderBy(xd => xd).ToArray()[0] == key.OrderBy(xd => xd).ToArray()[0]).ToList();
            var pairs = new List<double[]>();

            for (var i = 0; i < otherItems.Count; i++)
            {
                var keyOther = otherItems[i].Key;
                var newValues = new List<double>();
                for (var j = 0; j < keyOther.Count(); j++)
                {
                    if (!key.Contains(keyOther[j]))
                        newValues.Add(keyOther[j]);
                }
                var newPair = key.Concat(newValues).ToArray().OrderBy(x => x).ToArray();
                if ((!IsPairExist(pairs.ToArray(), newPair) && newPair.Count() > 2))
                    pairs.Add(newPair);
            }

            return pairs.ToArray();
        }

        /// <summary>
        ///     Checks if pair is exist or not
        /// </summary>
        /// <param name="list">List for checking pair</param>
        /// <param name="values">Values to look for pair</param>
        private static bool IsPairExist(IReadOnlyList<double[]> list, double[] values)
        {
            for (var i = 0; i < list.Count(); i++)
            {
                if (list[i].OrderBy(x => x).SequenceEqual(values.OrderBy(x => x)))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///     Gets counts of tthe item in the origin matrix
        /// </summary>
        /// <param name="val">Value to count</param>
        private int GetCount(IReadOnlyCollection<double> val)
        {
            var count = 0;
            for (var i = 0; i < _dataSet.Count(); i++)
            {
                var index = val.Count(t => _dataSet[i].Contains(t));
                if (index == val.Count)
                    count += 1;
            }
            return count;
        }

        /// <summary>
        ///     Gets counts of tthe item in the origin matrix
        /// </summary>
        /// <param name="val">Value to count</param>
        private double[][] GetTransactions(IReadOnlyCollection<double> val)
        {
            var transactionItems = new List<double[]>();
            for (var i = 0; i < _dataSet.Count(); i++)
            {
                var index = val.Count(t => _dataSet[i].Contains(t));
                if (index == val.Count)
                    transactionItems.Add(_dataSet[i]);
            }
            return transactionItems.ToArray();
        }

        /// <summary>
        ///     Gets counts of tthe item in the origin matrix
        /// </summary>
        /// <param name="list">List to check</param>
        /// <param name="val">Value to count</param>
        private static IEnumerable<double[]> GetCommonTransactions(IReadOnlyList<double[]> list,
            IReadOnlyCollection<double> val)
        {
            var transactionItems = new List<double[]>();
            for (var i = 0; i < list.Count(); i++)
            {
                var index = val.Count(t => list[i].Contains(t));
                if (index == val.Count)
                    transactionItems.Add(list[i]);
            }
            return transactionItems.ToArray();
        }
        #endregion
    }
}