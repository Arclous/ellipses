﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Ellipses.Helpers;
using Ellipses.Interfaces;
using Ellipses.Metrics;
using Ellipses.Models;

namespace Ellipses.Algorithms
{
    public class NaiveBayesPredicter : INaiveBayesPredicter
    {
        //Helper class for converting models
        private readonly IConverter _converter;

        //Probabilities of the features
        private readonly List<NaiveBayesProbability> _featureProbabilities;

        //Matrix of labels
        private readonly Matrix _labelMatrix;

        //Probabilities of the labels
        private readonly Dictionary<double, double> _labelProbabilities;

        /// <summary>
        ///     Naive Bayes Predicter
        /// </summary>
        /// <param name="labelProbabilities">Probability set for the labels</param>
        /// <param name="featureProbabilities">Probability set for the features</param>
        /// <param name="labelMatrix">Matrix of label</param>
        public NaiveBayesPredicter(Dictionary<double, double> labelProbabilities,
            List<NaiveBayesProbability> featureProbabilities, Matrix labelMatrix)
        {
            _converter = new Converter();
            _labelProbabilities = labelProbabilities;
            _featureProbabilities = featureProbabilities;
            _labelMatrix = labelMatrix;
        }

        /// <summary>
        ///     Predicts according to model
        /// </summary>
        /// <param name="model">Model for prediction label</param>
        public double Predict<T>(T model)
        {
            var probabilities = new Dictionary<double, double>();
            var modelConverted = _converter.ConvertModel(model);
            var dimensionalProcess = _converter.IsDimensional(model);
            var dimensionalValues = new List<double[]>();
            if (dimensionalProcess)
            {
                var orjModelValues = new List<double[]>();
                var newList = new List<double[]>();
                var dimensionalFeatures = _converter.ConvertDimensionalModel(model);

                for (var modelValuesRow = 0; modelValuesRow < modelConverted.Length; modelValuesRow++)
                    orjModelValues.Add(modelConverted[modelValuesRow].ToArray());

                for (var dimensionalRow = 0; dimensionalRow < dimensionalFeatures.Length; dimensionalRow++)
                    dimensionalValues.Add(dimensionalFeatures[dimensionalRow].ToArray());

                for (var dimensionalRow = 0; dimensionalRow < dimensionalFeatures.Length; dimensionalRow++)
                {
                    for (var modelRow = 0; modelRow < orjModelValues.Count; modelRow++)
                    {
                        newList.Add(orjModelValues[modelRow].Concat(dimensionalFeatures[dimensionalRow]).ToArray());
                    }
                }

                modelConverted = newList.ToArray();
            }
            var lbls = _labelMatrix.GetRows().Select(x => x[0]).Distinct().ToList();
            for (var i = 0; i < _labelProbabilities.Count; i++)
            {
                var value = 1.0;
                for (var j = 0; j < modelConverted[0].Count(); j++)
                {
                    var featureP =
                        _featureProbabilities.FirstOrDefault(
                            x => x.FeatureIndex == j && x.Feature == modelConverted[0][j] && x.Label == lbls[i])
                                             .FeatureProbability;
                    value = value*featureP;
                }
                value = value*_labelProbabilities[Convert.ToDouble(i)];
                probabilities.Add(lbls[i], value);
            }
            return probabilities.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
        }
    }
}