﻿using Ellipses.Algorithms;

namespace Ellipses.Interfaces
{
    public interface INaiveBayes
    {
        void LoadDataSet<T>(T[] models, bool normalization = false);
        INaiveBayesPredicter Train();
        void SaveModel(string fileName);
        INaiveBayesPredicter LoadModel(string fileName);
    }
}