﻿using System.Collections.Generic;
using Ellipses.Metrics;

namespace Ellipses.Interfaces
{
    public interface INormalizer
    {
        double[][] Normalize(double[][] dataSet);
        Matrix NormalizeAsMatrix(double[][] dataSet);
        Matrix MatrixNormalization(Matrix dataSet);
        double[] NormalizeInput(IReadOnlyList<double> y);
    }
}