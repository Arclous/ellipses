﻿using System.Collections.Generic;
using Ellipses.Metrics;

namespace Ellipses.Interfaces
{
    public interface IDistanceCalculater
    {
        double CalculateDistance(IReadOnlyList<double> x, IReadOnlyList<double> y, int length);
        double CalculateDistance(Vector vector, Vector y, int length);
    }
}