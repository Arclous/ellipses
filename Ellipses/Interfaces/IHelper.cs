﻿using System;
using System.Collections.Generic;
using Ellipses.Metrics;
using Ellipses.Models;

namespace Ellipses.Interfaces
{
    public interface IHelper
    {
        double[][] BuildMatrixAsArray(int rows, int columns);
        Matrix BuildMatrix(int rows, int columns);
        int[] InitializeClustering(int numData, int clusterCount, int seed);

        Dictionary<string, List<T>> OrganizeClustersWithLabel<T>(ref KMeansResults<T> result, string[] labels,
            Func<int, int> order = null);

        double Variance(double[] data, double mean);
        double Gauss(double x, double mean, double std);
        void SaveModel<T>(string fileName, T model);
        T LoadModel<T>(string fileName);
    }
}