﻿using System;
using System.Collections.Generic;
using Ellipses.Definations;
using Ellipses.Models;

namespace Ellipses.Interfaces
{
    public interface IKMeansCluster
    {
        void LoadDataSet<T>(T[] models, bool normalization = false);

        KMeansResults<T> Cluster<T>(int clusterCount, int maxIterations, int randomSeed = 0,
            int[] initialCentroidIndices = null);

        Dictionary<string, List<T>> OrganizeClustersWithLabel<T>(ref KMeansResults<T> result, string[] labels,
            Func<int, int> order = null);

        ElbowResult ApplyElbow<T>(ElbowOrigin origin, int max = 0, int maxIterations = 10);
    }
}