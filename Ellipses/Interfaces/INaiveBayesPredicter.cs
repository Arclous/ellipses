﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellipses.Interfaces
{
    public interface INaiveBayesPredicter
    {
        double Predict<T>(T model);
    }
}
