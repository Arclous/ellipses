﻿using System.Collections.Generic;
using Ellipses.Metrics;

namespace Ellipses.Interfaces
{
    public interface IConverter
    {
        double[][] ConvertModels<T>(IReadOnlyList<T> items);
        double[][] ConvertDimensionalModels<T>(IReadOnlyList<T> items);
        double[][] ConvertModel<T>(T model);
        double[][] ConvertDimensionalModel<T>(T model);
        Matrix ConvertModelsAsMatrix<T>(IReadOnlyList<T> items);
        Matrix ConvertLabelsAsMatrix<T>(IReadOnlyList<T> items);
        bool IsDimensionalFieldExist<T>(IReadOnlyList<T> items);
        bool IsDimensional<T>(T item);
    }
}