﻿namespace Ellipses.Interfaces
{
    public interface IApriori
    {
        void LoadDataSet<T>(T[] models);
        void SaveModel(string fileName);

        IAprioriPredicter Train();
    }
}