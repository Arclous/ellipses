﻿using System.Collections;

namespace Ellipses.Interfaces
{
    public interface IKNearestNeighbour
    {
        void LoadDataSet<T>(T[] models, bool normalization = false);

        IList GetNearestNeighbours(double[] y, int k,
            bool substractOrigin = false, IDistanceCalculater distanceCalculater = null);
    }
}