﻿using Ellipses.Models;

namespace Ellipses.Interfaces
{
    public interface IAprioriPredicter
    {
        AprioriResultModel Predict(double[] val);
    }
}