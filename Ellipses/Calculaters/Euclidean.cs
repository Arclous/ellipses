﻿/* ========================================================================
 * Ellipses Machine Learning Library 1.0
 * http://www.ellipsesai.com
 * ========================================================================
 * Copyright Ali Gulum
 *
 * ========================================================================
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://creativecommons.org/licenses/by-nc/4.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

using System;
using System.Collections.Generic;
using Ellipses.Interfaces;
using Ellipses.Metrics;

namespace Ellipses.Calculaters
{
    public class Euclidean : IDistanceCalculater
    {
        /// <summary>
        ///     Calculate distance with Euclidean Distance
        /// </summary>
        /// <param name="x">Data set</param>
        /// <param name="y">Data to check</param>
        /// <param name="length">Features length</param>
        public double CalculateDistance(IReadOnlyList<double> x, IReadOnlyList<double> y, int length)
        {
            var distance = 0d;
            for (var j = 0; j <= length; j++)
            {
                if (double.IsNaN((dynamic) x[j]) || double.IsNaN((dynamic) y[j])) continue;
                distance += Math.Pow(y[j] - x[j], 2);
            }
            return Math.Sqrt(distance);
        }

        /// <summary>
        ///     Calculate distance with Euclidean Distance
        /// </summary>
        /// <param name="vector">Data set as vector</param>
        /// <param name="y">Vector to check</param>
        /// <param name="length">Features length</param>
        public double CalculateDistance(Vector vector, Vector y, int length)
        {
            var distance = 0d;
            var x = vector.ToArray();
            for (var j = 0; j <= length; j++)
            {
                if (double.IsNaN((dynamic) x[j]) || double.IsNaN((dynamic) y[j])) continue;
                distance += Math.Pow(y[j] - x[j], 2);
            }
            return Math.Sqrt(distance);
        }
    }
}